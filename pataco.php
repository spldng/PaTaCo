<!DOCTYPE html>
<html>
	<head>
		<!-- CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fomantic-ui/2.8.7/semantic.min.css" />

		<!-- SCRIPTS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/fomantic-ui/2.8.7/semantic.min.js"></script>
	</head>
	<body>

		<div class="ui text raised container segment">
			<a class="ui teal ribbon label" href="https://twitter.com/spldngtdr" target="_blank">CC BY-SA 3.0 | Schelbie
        <i class="twitter icon"></i>
      </a>

			<h1 class="ui center aligned icon header">
				<i class="th icon"></i>
				PaTaCo
			</h1>
			<h2 class="ui center aligned header">Padlet-TaskCards-Converter</h2>
			<div class="ui container left aligned">
				<form class="ui form" action="padletJSON.php" method="post">
					<div class="field">
						<label>Padlet-Link</label>
						<div class="ui icon input">
							<input type="text" name="padletLink" placeholder="Padlet-Link" />
							<div class="ui icon button" data-tooltip="Das Padlet muss öffentlich zugänglich sein." data-position="right center" data-inverted="">
								<i class="question circle outline link icon"></i>
							</div>
						</div>
					</div>
					<button class="ui primary button" type="submit">Abschicken</button>
				</form>
			</div>
		</div>
	</body>
</html>
