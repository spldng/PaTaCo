# PaTaCo

Ein Converter, der Inhalte einer Padlet-Pinnwände in eine für TaskCards nutzbare JSON-Datei konvertiert. Durch den Import der JSON-Datei in TaskCards können die Inhalte von Padlet leicht nach TaskCards übertragen werden.

## Quickstart

0. Lokalen Webserver starten (z. B. XAMPP) und die Datei ``pataco.php`` ausführen.

1. Link der Padlet-Pinnwand einfügen.
##### Beachten: Die Padlet-Pinnwand muss öffentlich zugänglich sein!

2. Auf "Abschicken" klicken.

3. JSON-Datei speichern und in TaskCards importieren.

##### Tritt bei der Erstellung der JSON-Datei ein Fehler auf, wird dieser zurzeit noch in die JSON-Datei selbst geschrieben. Ein Import in TaskCards schlägt dann fehl.

## Welche Daten werden konvertiert?

Übernommen werden folgende Daten:
* Titel der Pinnwand
* Beschreibung der Pinnwand
* Titel der Spalte (Wall (Padlet) bzw. Liste (TaskCards))
* Titel des jeweiligen Eintrags (Wish (Padlet))
* Text des jeweiligen Eintrags
* Anhang des jeweiligen Eintrags (Bild, Link, Datei etc.)
* Farben der Pinnwand, der Spalten und der Einträge (Standard: weiß)
* Likes[^hint]
* Kommentare[^hint]

[^hint]: Likes und Kommentare scheinen zurzeit nicht in TaskCards importierbar zu sein. Selbst bei einem TaskCards-Export und Reimport gehen die Daten zu Likes und Kommentaren verloren.


## Bitte beachten

##### Nur Pinnwände vom Typ "Wall" können zurzeit übernommen werden.

Dateien, die in Padlet hochgeladen wurden, werden in TaskCards nur verlinkt! Werden die Daten von Padlet gelöscht, ist die Verlinkung in TaskCards nicht mehr gegeben.
