<?PHP
  $padletURL = $_POST['padletLink'];

  $padletHTML = file_get_contents($padletURL);
  $startingStatePos = strpos($padletHTML, '/api/1/padlet_starting_state?');
  $startingStateEnd = strpos($padletHTML, '" />', $startingStatePos);

  $substrLen = $startingStateEnd-$startingStatePos;
  $startingState = substr($padletHTML, $startingStatePos, $substrLen);

  $padlet = json_decode(file_get_contents("https://padlet.com".$startingState),true);
  //var_dump($padlet);
  //$padletJSON = JSON.stringify($padlet);

  $tkJson = [];

  $wallID = $padlet["wall"]["id"];

  $padletWall = json_decode(file_get_contents("https://padlet.com/api/5/wall_sections?wall_id=".$wallID),true);
  $padletWishes = json_decode(file_get_contents("https://api.padlet.com/api/5/wishes?wall_id=".$wallID),true);
  $padletComments = json_decode(file_get_contents("https://api.padlet.com/api/5/comments?wall_id=".$wallID),true);
  $padletLikes = json_decode(file_get_contents("https://api.padlet.com/api/5/reactions?wall_id=".$wallID),true);

  //  GRUNDLEGENDE INFORMATIONEN
  $tkJson['name'] = $padlet["wall"]["title"];
  $tkJson['description'] = $padlet["wall"]["description"];
  $wishCommentable = $padlet["wall"]["is_commentable"];

  //  KOMMENTARE

  $tkComments = [];

  if (sizeOf($padletComments["data"]) > 0) {
    foreach($padletComments["data"] as $comment) {
      $commentWishID = $comment["attributes"]["wish_id"];

      if(!isset($tkComments[$commentWishID])) {
        $commentSize = 0;
      }

      else {
        $commentSize = sizeOf($tkComments[$commentWishID]);
      }

      $tkComments[$commentWishID][$commentSize] = [
        "id" =>  intval($comment["id"]),
        "text" => $comment["attributes"]["body"]
      ];
    }
  }

  //  LIKES

  $tkLikes = [];

  if(sizeOf($padletLikes["data"]) > 0) {
    foreach($padletLikes["data"] as $like) {
      if($like["attributes"]["reaction_type"] == "like") {
        $tkLikes[$like["attributes"]["wish_id"]] = $like["attributes"]["value"];
      }
    }
  }

  //  WISHES/EINZELNE CARDS

  $tkWishes = [];

  foreach($padletWishes["data"] as $wall_wishes => $wish) {

    //  Manuelle Korrektur leerer Werte
    if($wish["attributes"]["headline"] == "Empty") {
      $wishTitle = "";
    }

    else {
      $wishTitle = $wish["attributes"]["headline"];
    }

    if($wish["attributes"]["color"] == null) {
      $wishColor = "#FFFFFF";
    }

    else {
      $wishColor = $wish["attributes"]["color"];
    }

    if($wish["attributes"]["attachment"] == null) {
      $wishLink = "";
    }

    else {
      $wishLink = $wish["attributes"]["attachment"];
    }

    if(isset($tkComments[$wish["id"]])) {
      $wishComments = $tkComments[$wish["id"]];
    }

    else {
      $wishComments = [];
    }

    if(isset($tkLikes[$wish["id"]])) {
      $wishLikes = $tkLikes[$wish["id"]];
    }

    else {
      $wishLikes = 0;
    }

    if(!isset($tkWishes[$wish["attributes"]["wall_section_id"]])) {
      $wishSize = 0;
    }

    else {
      $wishSize = sizeof($tkWishes[$wish["attributes"]["wall_section_id"]]);
    }

    $tkWishes[$wish["attributes"]["wall_section_id"]][$wishSize] = [
      "title" => $wishTitle,
      "description" => $wish["attributes"]["body"],
      "color" => $wishColor,
      "position" => $wish["attributes"]["sort_index"],
      "link" => $wishLink,
      "wallID" => $wish["attributes"]["wall_section_id"],
      "enableComment" => $wishCommentable,
      "comments" => $wishComments,
      "likes" => $wishLikes
    ];
  }

  foreach($tkWishes as $wishes) {
    $wallSectionID = $wishes[0]["wallID"];
    $sorting = array_column($wishes, "position");
    array_multisort($sorting, SORT_DESC, $wishes);
    $tkWishes[$wallSectionID] = $wishes;

    $sortIndex = 0;
    foreach($wishes as $wish) {
      $wish["position"] = $sortIndex;
      $tkWishes[$wallSectionID][$sortIndex] = $wish;
      $sortIndex++;
    }
  }

  //  WALLS/SPALTEN
  $tkLists = [];
  $listCounter = 0;

  foreach($padletWall["data"] as $wall_sections => $wall) {
    $wallID = $wall["id"];

    if(!isset($tkWishes[$wallID])) {
      $notices = [];
    }

    else {
      $notices = $tkWishes[$wallID];
    }

    if($wall["attributes"]["color"] == null) {
      $wallColor = "#FFFFFF";
    }

    else {
      $wallColor = $wall["attributes"]["color"];
    }

    $tkLists[$listCounter] = [
      "id" => $wall["id"],
      "name" => $wall["attributes"]["title"],
      "color" => $wallColor,
      "position" => $wall["attributes"]["sort_index"],
      "notices" => $notices
    ];

    $listCounter++;
  }

  $tkJson['lists'] = $tkLists;

  header('Content-disposition: attachment; filename=TaskCards.json');
  header('Content-type: application/json');
  echo json_encode($tkJson);
?>
